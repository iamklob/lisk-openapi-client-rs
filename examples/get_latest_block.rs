use std::error::Error;

use lisk_openapi_client::apis::{blocks_api, configuration::Configuration};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();
    let config = Configuration {
        base_path: String::from("https://node01.lisk.io/api"),
        ..Default::default()
    };
    let blocks = blocks_api::get_blocks(&config, None, None, None, None, None, None, None, None)
        .await
        .unwrap();
    println!("Blocks {:#?}", blocks);
    Ok(())
}
