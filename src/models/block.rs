/*
 * Lisk API Documentation
 *
 * # Welcome!  ## Access Restrictions The API endpoints are by default restricted to a whitelist of IPs that can be found in `config.json` in the section [`api.access.whitelist`](https://github.com/LiskHQ/lisk/blob/1.0.0/config.json#L35). If you want your API to be accessable by the public, you can do this by changing `api.access.public` to `true`. This will allow anyone to make requests to your Lisk Core node. However some endpoints stay private, that means only a list of whitelisted IPs can successfully make API calls to that particular endpoint; This includes all forging related API calls. By default, only the nodes' local IP is included in the whitelist, you can change the setting in `config.json` in the section [`forging.access.whitelist`](https://github.com/LiskHQ/lisk/blob/1.0.0/config.json#L114). For more details, see the descriptions at the respective endpoint.  ## Requests Chained filter parameters are logically connected with `AND`. `HTTP` is the supported URL schema by default. To enable `HTTPS`, please adjust the the [`ssl`](https://github.com/LiskHQ/lisk/blob/1.0.0/config.json#L124) section in `config.json`.  ## Responses The general response format is JSON (`application/json`). The responses for each API request have a common basic structure: ```javascript {  \"data\": {}, //Contains the requested data  \"meta\": {}, //Contains additional metadata, e.g. the values of `limit` and `offset`  \"links\": {} //Will contain links to connected API calls from here, e.g. pagination links } ```  ## Date Formats Most of the timestamp parameters are in the Lisk Timestamp format, which is similar to the Unix Timestamp format. The **Lisk Timestamp** is the number of seconds that have elapsed since the Lisk epoch time (2016-05-24T17:00:00.000Z), not counting leap seconds. The **Lisk Epoch Time** is returned in the [ISO8601](https://en.wikipedia.org/wiki/ISO_8601) format, combined date and time: `YYYY-MM-DDThh:mm:ssZ`. For details, see the descriptions and examples at the respective endpoint.  ## Pagination One can paginate nicely through the results by providing `limit` and `offset` parameters to the requests. `limit` and `offset` can be found in the `meta`-object of the response of an API request. If no limit and offset are provided, they are set to 10 and 0 by default, what will display the first 10 results.  ## List of Endpoints All possible API endpoints for Lisk Core are listed below. Click on an endpoint to show descriptions, details and examples. 
 *
 * The version of the OpenAPI document: 1.0.32
 * Contact: admin@lisk.io
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Block {
    /// Unique identifier of the block. Derived from the block signature. 
    #[serde(rename = "id")]
    pub id: String,
    /// Versioning for future upgrades of the lisk protocol.
    #[serde(rename = "version", skip_serializing_if = "Option::is_none")]
    pub version: Option<i32>,
    /// Height of the network, when the block got forged. The height of the networks represents the number of blocks, that have been forged on the network since Genesis Block. 
    #[serde(rename = "height")]
    pub height: i32,
    /// Unix Timestamp
    #[serde(rename = "timestamp")]
    pub timestamp: i32,
    /// Lisk Address of the delegate who forged the block.
    #[serde(rename = "generatorAddress", skip_serializing_if = "Option::is_none")]
    pub generator_address: Option<String>,
    /// Public key of th edelagte who forged the block.
    #[serde(rename = "generatorPublicKey")]
    pub generator_public_key: String,
    /// Bytesize of the payload hash.
    #[serde(rename = "payloadLength", skip_serializing_if = "Option::is_none")]
    pub payload_length: Option<i32>,
    /// Hash of the payload of the block. The payload of a block is comprised of the transactions the block contains. For each type of transaction exists a different maximum size for the payload. 
    #[serde(rename = "payloadHash", skip_serializing_if = "Option::is_none")]
    pub payload_hash: Option<String>,
    /// Derived from a SHA-256 hash of the block header, that is signed by the private key of the delegate who forged the block. 
    #[serde(rename = "blockSignature", skip_serializing_if = "Option::is_none")]
    pub block_signature: Option<String>,
    /// Number of times that this Block has been confirmed by the network. By forging a new block on a chain, all former blocks in the chain get confirmed by the forging delegate. 
    #[serde(rename = "confirmations", skip_serializing_if = "Option::is_none")]
    pub confirmations: Option<i32>,
    /// The id of the previous block of the chain.
    #[serde(rename = "previousBlockId", skip_serializing_if = "Option::is_none")]
    pub previous_block_id: Option<String>,
    /// The number of transactions processed in the block.
    #[serde(rename = "numberOfTransactions")]
    pub number_of_transactions: i32,
    /// The total amount of Lisk transferred.
    #[serde(rename = "totalAmount")]
    pub total_amount: String,
    /// The total amount of fees associated with the block.
    #[serde(rename = "totalFee")]
    pub total_fee: String,
    /// The Lisk reward for the delegate.
    #[serde(rename = "reward")]
    pub reward: String,
    /// Total amount of LSK that have been forged in this Block. Consists of fees and the reward. 
    #[serde(rename = "totalForged")]
    pub total_forged: String,
}

impl Block {
    pub fn new(id: String, height: i32, timestamp: i32, generator_public_key: String, number_of_transactions: i32, total_amount: String, total_fee: String, reward: String, total_forged: String) -> Block {
        Block {
            id,
            version: None,
            height,
            timestamp,
            generator_address: None,
            generator_public_key,
            payload_length: None,
            payload_hash: None,
            block_signature: None,
            confirmations: None,
            previous_block_id: None,
            number_of_transactions,
            total_amount,
            total_fee,
            reward,
            total_forged,
        }
    }
}


